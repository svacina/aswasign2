
## Assignment 2

Implementation of tests in `admin` & `admin-client`.

cd chapter4/admin

Run

```$xslt
mvn test
```

cd chapter4/admin-client

```$xslt
mvn test
```

