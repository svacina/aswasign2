package ejm.adminclient;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

import au.com.dius.pact.consumer.ConsumerPactTestMk2;
import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.PactSpecVersion;
import au.com.dius.pact.model.RequestResponsePact;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.http.client.fluent.Response;
import org.fest.assertions.Assertions;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Ken Finnigan
 */

//LocalDateTime.parse("2002-01-01T00:00:00")

public class ConsumerPactTest extends ConsumerPactTestMk2 {
    private Category createCategory(Integer id, String name) {
        Category cat = new TestCategoryObject(id, LocalDateTime.parse("2002-01-01T00:00:00"), 1);
        cat.setName(name);
        cat.setVisible(Boolean.TRUE);
        cat.setHeader("header");
        cat.setImagePath("n/a");

        return cat;
    }

    private Category createCategory(Integer id, String name, Category parent) {
        Category cat = new TestCategoryObject(id, LocalDateTime.parse("2002-01-01T00:00:00"), 1);
        cat.setName(name);
        cat.setVisible(Boolean.TRUE);
        cat.setHeader("header");
        cat.setImagePath("n/a");
        cat.setParent(parent);
        return cat;
    }

    @Override
    protected RequestResponsePact createPact(PactDslWithProvider builder) {
        Category top = createCategory(0, "Top");

        Category transport = createCategory(1000, "Transportation");
        transport.setParent(top);

        Category autos = createCategory(1002, "Automobiles");
        autos.setParent(transport);

        Category cars = createCategory(1009, "Cars");
        cars.setParent(autos);

        Category toyotas = createCategory(1015, "Toyota Cars");
        toyotas.setParent(cars);

        ObjectMapper mapper = new ObjectMapper()
                .registerModule(new JavaTimeModule());

        try {
            builder
                    .uponReceiving("Retrieve a category")
                        .path("/admin/category/1015")
                        .method("GET")
                    .willRespondWith()
                        .status(200)
                        .body(mapper.writeValueAsString(toyotas))
                    .toPact();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        //ALL
        Category bikes = createCategory(1001, "Bikes", transport);
        Category trucks = createCategory(1010, "Trucks");
        Category suvs = createCategory(1011, "SUVs");

        ArrayList<Category> collection = new ArrayList<>();
        collection.add(top);
        collection.add(transport);
        collection.add(bikes);
        collection.add(autos);
        Category roadBikes = (createCategory(1003, "Road Bikes", bikes));
        collection.add(roadBikes);
        collection.add(createCategory(1004, "Mountain Bikes", bikes));
        collection.add(createCategory(1005, "Cannondale Road", roadBikes));
        collection.add(createCategory(1006, "Schwinn Road", roadBikes));
        collection.add(createCategory(1007, "Cannondale Mountain", bikes));
        collection.add(createCategory(1008, "Schwinn Mountain", bikes));
        collection.add(createCategory(1012, "Ford Cars", cars));
        collection.add(createCategory(1013, "Ford Trucks", trucks));
        collection.add(createCategory(1014, "Ford SUVs", suvs));
        collection.add(createCategory(1015, "Toyota Cars", cars));
        collection.add(createCategory(1016, "Toyota Trucks", trucks));
        collection.add(createCategory(1017, "Toyota SUVs", suvs));
        collection.add(createCategory(1018, "Audi", cars));
        collection.add(createCategory(1019, "Porsche", cars));


        try {
            builder
                    .uponReceiving("Retrieve a category collection")
                    .path("/admin/category")
                    .method("GET")
                    .willRespondWith()
                    .status(200)
                    .body(mapper.writeValueAsString(collection))
                    .toPact();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        //UPDATE

        toyotas.setName("Toyotas Super Cars");
        toyotas.setParent(null);

        try {
            return builder
                    .uponReceiving("Update the category")
                    .path("/admin/category/1015")
                    .method("PUT")
                    .body(mapper.writeValueAsString(toyotas))
                    .willRespondWith()
                    .status(200)
                    .body(mapper.writeValueAsString(toyotas))
                    .toPact();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
//
//        // CREATE
//
//        try {
//            return builder
//                    .uponReceiving("Create the category")
//                    .path("/admin/category")
//                    .method("POST")
//                    .body(toyotas.toString())
//                    .willRespondWith()
//                    .status(200)
//                    .body(mapper.writeValueAsString(collection))
//                    .toPact();
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }

        return null;
    }

    @Override
    protected String providerName() {
        return "admin_service_provider";
    }

    @Override
    protected String consumerName() {
        return "admin_client_consumer";
    }

    @Override
    protected PactSpecVersion getSpecificationVersion() {
        return PactSpecVersion.V3;
    }

    @Override
    protected void runTest(MockServer mockServer) throws IOException {
        AdminClient adminClient = new AdminClient(mockServer.getUrl());
        Category cat = adminClient.getCategory(1015);

        Assertions.assertThat(cat).isNotNull();
        assertThat(cat.getId()).isEqualTo(1015);
        assertThat(cat.getName()).isEqualTo("Toyota Cars");
        assertThat(cat.getHeader()).isEqualTo("header");
        assertThat(cat.getImagePath()).isEqualTo("n/a");
        assertThat(cat.isVisible()).isTrue();
        assertThat(cat.getParent()).isNotNull();
        assertThat(cat.getParent().getId()).isEqualTo(1009);

        // ALL
        Collection<Category> all = adminClient.getAllCategories();
        assertThat(all.size()).isEqualTo(18);

//        //Update
//        Category cars = createCategory(1009, "Cars");
//        Category toyotas = createCategory(1015, "Toyota Cars");
//        toyotas.setParent(cars);

        //Update
        Category cars = createCategory(1009, "Cars");
        Category toyotas = createCategory(1015, "Toyotas Super Cars");
        toyotas.setParent(null);
        String  r = new AdminClient(mockServer.getUrl()).updateCategory(toyotas);
//        cat = new AdminClient(mockServer.getUrl()).updateCategory(toyotas);
//
//        assertThat(cat.getName()).isEqualTo("Toyota Cars");
//        assertThat(cat.getHeader()).isEqualTo("header");
//        assertThat(cat.getImagePath()).isEqualTo("n/a");
//        assertThat(cat.isVisible()).isTrue();
//        assertThat(cat.getParent()).isNotNull();
//        assertThat(cat.getParent().getId()).isEqualTo(1009);

//        assertThat(r.returnResponse().getStatusLine().getStatusCode()).isEqualTo(200);
//
        //Create
//        toyotas = createCategory(1015, "Toyota Cars");
//        toyotas.setParent(cars);
//        r = new AdminClient(mockServer.getUrl()).addCategory(toyotas);
//        assertThat(r.returnResponse().getStatusLine().getStatusCode()).isEqualTo(200);
//        cat = new AdminClient(mockServer.getUrl()).getCategory(1015);
//        assertThat(cat.getName()).isEqualTo("Toyota Cars");
//        assertThat(cat.getHeader()).isEqualTo("header");
//        assertThat(cat.getImagePath()).isEqualTo("n/a");
//        assertThat(cat.isVisible()).isTrue();
//        assertThat(cat.getParent()).isNotNull();
//        assertThat(cat.getParent().getId()).isEqualTo(1009);
//
//        //Delete
//        r = new AdminClient(mockServer.getUrl()).removeCategory(1009);
//        assertThat(r.returnResponse().getStatusLine().getStatusCode()).isEqualTo(200);
//        cat = new AdminClient(mockServer.getUrl()).getCategory(1015);
//        assertThat(cat).isEqualTo(null);

    }
}
