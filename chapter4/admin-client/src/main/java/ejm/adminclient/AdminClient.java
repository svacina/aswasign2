package ejm.adminclient;

import java.io.*;
import java.net.URISyntaxException;
import java.util.Collection;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.*;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 * @author Ken Finnigan
 */
public class AdminClient {
    private String url;

    public AdminClient(String url) {
        this.url = url;
    }

    public Category getCategory(final Integer categoryId) throws IOException {
        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(url).setPath("/admin/category/" + categoryId);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        String jsonResponse =
                Request
                    .Get(uriBuilder.toString())
                    .execute()
                        .returnContent().asString();

        if (jsonResponse.isEmpty()) {
            return null;
        }

        return new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .readValue(jsonResponse, Category.class);
    }

    public Collection<Category> getAllCategories() throws IOException {
        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(url).setPath("/admin/category");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        Content content =
                Request
                        .Get(uriBuilder.toString())
                        .execute()
                        .returnContent();

        System.out.println(content.toString());

        String jsonResponse = content.toString();

        if (jsonResponse.isEmpty()) {
            return null;
        }

        Collection<Category> categories = new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .readValue(jsonResponse, new TypeReference<Collection<Category>>() {});
        return categories;
    }

    public Response addCategory(Category categoryToBeAdded) throws IOException {
        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(url).setPath("/admin/category");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        Response r =
                (Response) Request
                        .Post(uriBuilder.toString())
                        .bodyByteArray(categoryToBeAdded.toString().getBytes())
                        .execute()
                        .returnResponse();

        if (r == null) {
            return null;
        }

        return r;
    }
    //http://www.codepreference.com/2017/02/using-apache-httpclient-fluent-api.html
    public String updateCategory(Category categoryToBeAdded) throws IOException {
        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(url).setPath("/admin/category/" + categoryToBeAdded.getId());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);


        oos.writeObject(categoryToBeAdded);

        oos.flush();
        oos.close();

        InputStream is = new ByteArrayInputStream(baos.toByteArray());

        ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());

        //Object to JSON in file
        String json = mapper.writeValueAsString(categoryToBeAdded);

        //Object to JSON in String
//        String jsonInString = mapper.writeValueAsString(obj);

        Response content =
                Request
                        .Put(uriBuilder.toString())
                        .addHeader("Content-Type", "application/json")
                        .bodyString(json, ContentType.APPLICATION_JSON)
                        .execute();
        String r = content.toString();

//        BasicHttpEntity basicHttpEntity = new BasicHttpEntity();



//        Response r = (Response) Request.Put(uriBuilder.toString()).bodyStream(is).execute().returnResponse();
//        Response r = null;
//        CloseableHttpClient httpclient = HttpClients.createDefault();
//        HttpPost httppost = new HttpPost(uriBuilder.toString());
//        httppost.setEntity(new SerializableEntity(categoryToBeAdded));
//        try (CloseableHttpResponse response = httpclient.execute(httppost)) {
//            // do something with response
//            r = (Response) response;
//        }
//        Response r =
//                (Response) Request
//                        .Put(uriBuilder.toString())
////                        .body(new HttpEntityWrapper(categoryToBeAdded))
//                        .bodyByteArray(categoryToBeAdded.toString().getBytes())
//                        .execute()
//                        .returnResponse();

        if (r == null) {
            return null;
        }

        return r;
    }


    public Response removeCategory(final int categoryId) throws IOException {
        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(url).setPath("/admin/category/" + categoryId);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        Response r =
                (Response) Request
                        .Post(uriBuilder.toString())
                        .execute()
                        .returnResponse();

        if (r == null) {
            return null;
        }

        return r;
    }


}
